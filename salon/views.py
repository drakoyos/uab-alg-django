from multiprocessing import context
from django.shortcuts import render, redirect
from .forms import SalonForm
from curso.models import *

def index(request):
    context={'salones': Salon.objects.all()}
    return render(request, "salon/index.html", context)

def create(request):
    if request.method == 'POST':
        form = SalonForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('index_salon')
    else:
        form = SalonForm()
    context = {'form': form}
    return render(request, "salon/create.html", context)

def delete(request, salon_id):
    salon = Salon.objects.get(id=salon_id)
    salon.delete()
    return redirect('index_salon')

def update(request, salon_id):
    curso = Salon.objects.get(id=salon_id)
    if request.method == 'POST':
        form = SalonForm(request.POST, instance=curso)
        if form.is_valid():
            form.save()
            return redirect('index_salon')
    else:
        form = SalonForm(instance=curso)
    context = {'form': form}
    return render(request, "salon/edit.html", context)