from dataclasses import fields
from django import forms

from curso.models import Curso, Inscripcion

class CreateCurso(forms.ModelForm):
    class Meta:
        model = Curso
        fields = ['codigo','nombre', 'creditos', 'salon', 'universidad','docente']
        widgets = {
            'codigo': forms.TextInput(attrs={'class': 'form-control'}),
            'nombre': forms.TextInput(attrs={'class': 'form-control'}),
            'creditos': forms.NumberInput(attrs={'class': 'form-control'}),
            'universidad': forms.Select(attrs={'class': 'form-control'}),
            'salon': forms.Select(attrs={'class': 'form-control'}),
            'docente': forms.Select(attrs={'class': 'form-control'}),
        }
    # this function will be used for the validation
    def clean(self):

        # data from the form is fetched using super function
        super(CreateCurso, self).clean()

        # extract the data
        codigo = self.cleaned_data.get('codigo', '')
        nombre = self.cleaned_data.get('nombre', '')

        # conditions to be met for the username length
        if len(codigo) < 3 or len(codigo) > 8:
            self._errors['codigo'] = self.error_class([
                'El código debe tener entre 3 a 8 caracteres'])
        if len(nombre) < 5 or len(nombre) > 40:
            self._errors['nombre'] = self.error_class([
                'El nombre debe tener entre 5 a 40 caracteres'])

        # return any errors if found
        return self.cleaned_data

# class InscripcionForm(forms.ModelForm):
#     class Meta:
#         model = Inscripcion
#         fields = ['estudiante', 'curso']
#         widgets = {
#             'estudiante': forms.Select(attrs={'class': 'form-control'}),
#             'curso': forms.Select(attrs={'class': 'form-control'}),
#         }
