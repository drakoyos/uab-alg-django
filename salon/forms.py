from dataclasses import fields
from django import forms

from curso.models import Salon

class SalonForm(forms.ModelForm):
    class Meta:
        model = Salon
        fields = "__all__"
        widgets = {
            'codigo': forms.TextInput(attrs={'class': 'form-control'}),
            'nombre': forms.TextInput(attrs={'class': 'form-control'}),
            'ubicacion': forms.TextInput(attrs={'class': 'form-control'}),
        }

    # this function will be used for the validation
    def clean(self):

        # data from the form is fetched using super function
        super(SalonForm, self).clean()

        # extract the data
        codigo = self.cleaned_data.get('codigo', '')
        nombre = self.cleaned_data.get('nombre', '')
        ubicacion = self.cleaned_data.get('ubicacion', '')

        # conditions to be met for the username length
        if len(codigo) < 3 or len(codigo) > 8:
            self._errors['codigo'] = self.error_class([
                'El código debe tener entre 3 a 8 caracteres'])
        if len(nombre) < 5 or len(nombre) > 40:
            self._errors['nombre'] = self.error_class([
                'El nombre debe tener entre 5 a 40 caracteres'])
        
        if len(ubicacion) < 5 or len(ubicacion) > 250:
            self._errors['email'] = self.error_class([
                'La ubicación debe tener entre 5 a 250 caracteres'])

        # return any errors if found
        return self.cleaned_data
