from django.urls import path
from . import views
urlpatterns = [
    path('', views.index, name='index'),
    path('<question_id>/', views.detail, name='detail'),
    path('<question_id>/results/', views.results, name='results'),
    path('<question_id>/vote/', views.vote, name='vote'),
    # Universidad
    path('univ/emp', views.emp),
    path('univ/show',views.show),
    path('univ/edit/<int:id>', views.edit),
    path('univ/update/<int:id>', views.update),
    path('univ/delete/<int:id>', views.destroy),
    # Programa
    path('prog/emp', views.prog_emp),
    path('prog/show',views.prog_show),
    path('prog/edit/<int:id>', views.prog_edit),
    path('prog/update/<int:id>', views.prog_update),
    path('prog/delete/<int:id>', views.prog_destroy),
]
