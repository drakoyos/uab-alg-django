from django.db import models

# Create your models here.
import datetime
from django.utils import timezone


class Question(models.Model):
    question_text = models.CharField(max_length=200)
    pub_date = models.DateTimeField("date published")

    def __str__(self):
        return self.question_text

    def was_published_recently(self):
        now = timezone.now()
        return now - datetime.timedelta(days=1) <= self.pub_date <= now

    was_published_recently.admin_order_field = 'pub_date'
    was_published_recently.boolean = True
    was_published_recently.short_description = 'Published recently?'


class Choice(models.Model):
    question = models.ForeignKey(Question, on_delete=models.DO_NOTHING, )
    choice_text = models.CharField(max_length=200)
    votes = models.IntegerField(default=0)

    def __str__(self):
        return self.choice_text


class Universidad(models.Model):
    nombre = models.CharField(max_length=100)
    sigla = models.CharField(max_length=10)
    email = models.EmailField()

    def __str__(self):
        return self.nombre + ' (' + self.sigla + ')'


class Programa(models.Model):
    codigo = models.CharField(max_length=8)
    nombre = models.CharField(max_length=100)
    departamento = models.CharField(max_length=50)
    descripcion = models.CharField(max_length=256)
    universidad_id = models.ForeignKey(Universidad, on_delete=models.DO_NOTHING, )

    def __str__(self):
        return self.nombre + ' (' + self.codigo + ')'


class Lista:
    def __init__(self):
        self.head = None

    def appendHead(self, dato):
        nuevoNodo = Nodo(dato)
        nuevoNodo.next = self.head
        self.head = nuevoNodo

    def getHead(self):
        return self.head

    def clearNode(self):
        self.head = None

    def insertNode(self, dato):
        newNode = Nodo(dato)
        newNode.next = self.head
        self.head = newNode
        return True

    def deleteNodePos(self, pos):
        if self.head is None:
            return
        if pos == 0:
            self.head = self.head.next
            return self.head
        idx = 0
        current = self.head
        prev = self.head
        temp = self.head
        while current is not None:
            if idx == pos:
                temp = current.next
                break
            prev = current
            current = current.next
            idx += 1
        prev.next = temp
        return prev

    def searchInList(self, item):
        current = self.head
        if current is None:
            print('Empty List')
            return
        else:
            output = []
            while current:
                if current.data.nombre == item:
                    output.append(current.data)
                    break
                current = current.next
            return output

    def countNodes(self):
        current = self.head
        i = 0
        while current:
            i = i + 1
            current = current.next
        return i

    def show(self):
        current = self.head
        listInfo = []
        while current:
            listInfo.append(current)
            # listInfo = listInfo + current.data.nombre
            current = current.next

        return listInfo
        # print(']', end=' ')


class Nodo:
    def __init__(self, data, next=None):
        self.data = data
        self.next = next

    def __str__(self):
        return str(self.data) + " " + str(self.next)
