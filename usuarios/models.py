from django.db import models
from fdj.models import Universidad

class Usuarios(models.Model):
    usuario = models.CharField(max_length=100)
    nombre = models.CharField(max_length=100)
    password = models.CharField(max_length=100)
    email = models.EmailField()
    universidad = models.ForeignKey(Universidad, null=False, blank=False, on_delete=models.CASCADE)
    
    def __str__(self):
        return self.nombre+' ('+self.universidad.nombre+')'
    
    class Meta:
        abstract = True

class Estudiantes(Usuarios):
    fechaDeNacimiento = models.DateField()
    numeroMatricula = models.CharField(max_length=50)
    
class Docentes(Usuarios):
    especialidad = models.CharField(max_length=150)
    pagoXHoras = models.FloatField()
    tipo = models.CharField(max_length=3)