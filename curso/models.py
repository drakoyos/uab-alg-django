from django.db import models
from fdj.models import Universidad
from usuarios.models import Docentes, Estudiantes

# Modelo momentaneo hasta tener el verdadero

class Salon(models.Model):
    codigo = models.CharField(max_length=8)
    nombre = models.CharField(max_length=40)
    ubicacion = models.CharField(max_length=250)
    def __str__(self):
        return self.nombre+' ('+self.codigo+')'
class Curso(models.Model):
    codigo = models.CharField(max_length=8)
    nombre = models.CharField(max_length=50)
    creditos = models.IntegerField()
    salon = models.ForeignKey(Salon, on_delete=models.CASCADE, related_name="courses")
    universidad = models.ForeignKey(Universidad, on_delete=models.CASCADE, related_name="courses")
    docente = models.ForeignKey(Docentes, on_delete=models.CASCADE, related_name="courses")
    def __str__(self):
        return self.nombre+' ('+self.codigo+')'

# Modelo para la relacion de estudiante y curso
class Inscripcion(models.Model):
    id = models.AutoField(primary_key=True)
    estudiante = models.ForeignKey(Estudiantes, null=False, blank=False, on_delete=models.CASCADE)
    curso = models.ForeignKey(Curso, null=False, blank=False, on_delete=models.CASCADE)
    fechaInsc = models.DateTimeField(auto_now_add=True)