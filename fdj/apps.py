from django.apps import AppConfig


class FdjConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'fdj'
