from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404, render
from django.urls import reverse
#
from django.shortcuts import render, redirect
from firstDjango.forms import UniversidadForm, ProgramaForm
from fdj.models import Universidad, Programa, Lista, Nodo

from .models import Question, Choice


def index(request):
    latest_question_list = Question.objects.order_by('-pub_date')[:5]
    context = {'latest_question_list': latest_question_list}
    return render(request, 'fdj/index.html', context)


def detail(request, question_id):
    question = get_object_or_404(Question, pk=question_id)
    return render(request, 'fdj/detail.html', {'question': question})


def results(request, question_id):
    question = get_object_or_404(Question, pk=question_id)
    return render(request, 'fdj/results.html', {'question': question})


def vote(request, question_id):
    question = get_object_or_404(Question, pk=question_id)
    try:
        selected_choice = question.choice_set.get(pk=request.POST['choice'])
    except (KeyError, Choice.DoesNotExist):
        return render(request, 'fdj/detail.html', {
            'question': question,
            'error_message': "You didn't select a choice.",
        })
    else:
        selected_choice.votes += 1
        selected_choice.save()
        return HttpResponseRedirect(reverse('fdj:results', args=(question.id,)))


# Universidad
def emp(request):
    if request.method == "POST":
        form = UniversidadForm(request.POST)
        if form.is_valid():
            try:
                form.save()
                return redirect('/fdj/univ/show')
            except:
                pass
        else:

            # Redirect back to the same page if the data was invalid
            return render(request, "fdj/univ/index.html", {'form': form})
    else:
        form = UniversidadForm()
    return render(request, 'fdj/univ/index.html', {'form': form})


def show(request):
    universidades = Universidad.objects.all()
    return render(request, "fdj/univ/show.html", {'universidades': universidades})

# def show(request):
#     universidades = Universidad.objects.all()
#     lista = Lista()
#     for universidad in universidades:
#         nuevoNodo = Nodo(universidad)
#         lista.appendHead(nuevoNodo)
#     return render(request, "fdj/univ/show.html", {'universidades': universidades, 'lista': lista.show()})


def edit(request, id):
    universidad = Universidad.objects.get(id=id)
    return render(request, 'fdj/univ/edit.html', {'universidad': universidad})


def update(request, id):
    universidad = Universidad.objects.get(id=id)
    form = UniversidadForm(request.POST, instance=universidad)
    if form.is_valid():
        form.save()
        return redirect("/fdj/univ/show")
    else:
        # Redirect back to the same page if the data was invalid
        return render(request, 'fdj/univ/edit.html', {'universidad': universidad, 'form': form})


def destroy(request, id):
    employee = Universidad.objects.get(id=id)
    employee.delete()
    return redirect("/fdj/univ/show")


# Programa
def prog_emp(request):
    if request.method == "POST":
        form = ProgramaForm(request.POST)
        if form.is_valid():
            try:
                form.save()
                return redirect('/fdj/prog/show')
            except:
                pass
        else:
            # Redirect back to the same page if the data was invalid
            return render(request, 'fdj/prog/index.html', {'form': form})
    else:
        form = ProgramaForm()
    return render(request, 'fdj/prog/index.html', {'form': form})


def prog_show(request):
    programas = Programa.objects.all()
    return render(request, "fdj/prog/show.html", {'programas': programas})


def prog_edit(request, id):
    programa = Programa.objects.get(id=id)
    universidades = Universidad.objects.all()
    return render(request, 'fdj/prog/edit.html', {'programa': programa, 'universidades': universidades})


def prog_update(request, id):
    programa = Programa.objects.get(id=id)
    universidades = Universidad.objects.all()
    form = ProgramaForm(request.POST, instance=programa)
    if form.is_valid():
        form.save()
        return redirect("/fdj/prog/show")
    else:
        # Redirect back to the same page if the data was invalid
        return render(request, 'fdj/prog/edit.html', {'programa': programa, 'universidades': universidades, 'form': form})


def prog_destroy(request, id):
    employee = Programa.objects.get(id=id)
    employee.delete()
    return redirect("/fdj/prog/show")
