from django.urls import path
from . import views

urlpatterns = [
    path("",views.index, name='index_salon'),
    path("create/", views.create, name='create_salon'),
    path("delete/<int:salon_id>/", views.delete, name='delete_salon'),
    path("update/<int:salon_id>/", views.update, name='update_salon')
]
