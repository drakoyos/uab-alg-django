from django import forms
from fdj.models import Universidad, Programa
from usuarios.models import Estudiantes, Docentes

class UniversidadForm(forms.ModelForm):
    class Meta:
        model = Universidad
        fields = "__all__"

    # this function will be used for the validation
    def clean(self):

        # data from the form is fetched using super function
        super(UniversidadForm, self).clean()

        # extract the data
        nombre = self.cleaned_data.get('nombre', '')
        sigla = self.cleaned_data.get('sigla', '')
        email = self.cleaned_data.get('email', '')

        # conditions to be met for the username length
        if len(nombre) < 5:
            self._errors['nombre'] = self.error_class([
                'El nombre debe tener al menos 5 caracteres'])
        if len(sigla) < 3:
            self._errors['sigla'] = self.error_class([
                'La sigla debe tener al menos 3 caracteres'])
        if len(email) < 6:
            self._errors['email'] = self.error_class([
                'Ingrese un email valido'])

        # return any errors if found
        return self.cleaned_data


class ProgramaForm(forms.ModelForm):
    class Meta:
        model = Programa
        fields = "__all__"
        widgets = {
            'descripcion': forms.Textarea(attrs={'rows': 3, 'cols': 100}),
        }

    # this function will be used for the validation
    def clean(self):

        # data from the form is fetched using super function
        super(ProgramaForm, self).clean()

        # extract the data
        codigo = self.cleaned_data.get('codigo', '')
        nombre = self.cleaned_data.get('nombre', '')
        departamento = self.cleaned_data.get('departamento', '')
        descripcion = self.cleaned_data.get('descripcion', '')
        universidad_id = self.cleaned_data.get('universidad_id', '')

        # conditions to be met for the username length
        if len(codigo) < 8:
            self._errors['codigo'] = self.error_class([
                'El codigo debe tener 8 caracteres'])
        if len(nombre) < 5:
            self._errors['nombre'] = self.error_class([
                'El nombre debe tener al menos 5 caracteres'])
        if len(departamento) < 10:
            self._errors['departamento'] = self.error_class([
                'El departamento debe tener al menos 10 caracteres'])
        if len(descripcion) < 15:
            self._errors['descripcion'] = self.error_class([
                'La departamento debe tener al menos 15 caracteres'])

        # univ = Universidad.objects.filter(id=universidad_id.id)
        # if not univ:  # check if any object exists
        #     self._errors['universidad_id'] = self.error_class([
        #         'La universidad no existe'])

            # return any errors if found
        return self.cleaned_data


class EstudiantesForm(forms.ModelForm):
    class Meta:
        model = Estudiantes
        fields = "__all__"

    def clean(self):

        super(EstudiantesForm, self).clean()

        # extract the data
        usuario = self.cleaned_data.get('usuario', '')
        nombre = self.cleaned_data.get('nombre', '')
        password = self.cleaned_data.get('password', '')
        email = self.cleaned_data.get('email', '')
        numeroMatricula = self.cleaned_data.get('tipo', '')
        universidad_id = self.cleaned_data.get('universidad_id', '')

        if len(usuario) < 4:
            self._errors['usuario'] = self.error_class([
                'El usuario debe tener al menos 4 caracteres'])
            
        if len(nombre) < 5:
            self._errors['nombre'] = self.error_class([
                'El Nombre debe tener al menos 5 caracteres'])
            
        if len(password) < 3:
            self._errors['password'] = self.error_class([
                'La Contraseña debe tener al menos 3 caracteres'])
            
        if len(email) < 6:
            self._errors['email'] = self.error_class([
                'Ingrese un email valido'])

        # return any errors if found
        return self.cleaned_data


class DocentesForm(forms.ModelForm):
    class Meta:
        model = Docentes
        fields = "__all__"

    def clean(self):

        super(DocentesForm, self).clean()

        # extract the data
        usuario = self.cleaned_data.get('usuario', '')
        nombre = self.cleaned_data.get('nombre', '')
        password = self.cleaned_data.get('password', '')
        email = self.cleaned_data.get('email', '')
        especialidad = self.cleaned_data.get('especialidad', '')
        pagoXHoras = self.cleaned_data.get('pagoXHoras', '')
        tipo = self.cleaned_data.get('tipo', '')
        universidad_id = self.cleaned_data.get('universidad_id', '')
        if len(usuario) < 4:
            self._errors['usuario'] = self.error_class([
                'El usuario debe tener al menos 4 caracteres'])
            
        if len(nombre) < 5:
            self._errors['nombre'] = self.error_class([
                'El Nombre debe tener al menos 5 caracteres'])
            
        if len(password) < 3:
            self._errors['password'] = self.error_class([
                'La Contraseña debe tener al menos 3 caracteres'])
            
        if len(email) < 6:
            self._errors['email'] = self.error_class([
                'Ingrese un email valido'])

        # return any errors if found
        return self.cleaned_data