from __future__ import print_function

#clase nodo
class Node:

    def __init__(self, label, parent):
        self.label = label
        self.left = None
        self.right = None
        self.parent = parent

def getLabel(self):
    return self.label

def setLabel(self, label):
    self.label = label

def getParent(self):
    return self.parent

def setParent(self, parent):
    self.parent = parent


# obtenemos nodo
def getNode(self, label):
        curr_node = None
        if(not self.empty()):
            while curr_node is not None and curr_node.getLabel() is not label:
                if label < curr_node.getLabel():
                    curr_node = curr_node.getLeft()
                else:
                    curr_node = curr_node.getRight()
        return curr_node

# Es vacia? 
def empty(self):
    if self.root is None:
        return True
    return False


# Operación de borrado
def delete(self, label):
    if (not self.empty()):
        node = self.getNode(label)
        if(node is not None):
            if(node.getLeft() is None and node.getRight() is None):
                self.__reassignNodes(node, None)
                node = None
            elif(node.getLeft() is None and node.getRight() is not None):
                self.__reassignNodes(node, node.getRight())
            elif(node.getLeft() is not None and node.getRight() is None):
                self.__reassignNodes(node, node.getLeft())
            else:
                tmpNode = self.getMax(node.getLeft())
                self.delete(tmpNode.getLabel())
                node.setLabel(tmpNode.getLabel())

# obtenemos el maximo
def getMax(self, root = None):
    if(root is not None):
        curr_node = root
    if(not self.empty()):
        while(curr_node.getRight() is not None):
            curr_node = curr_node.getRight()
    return curr_node

# obtenemos el minimo
def getMin(self, root = None):
    if(root is not None):
        curr_node = root

    if(not self.empty()):
        while(curr_node.getLeft() is not None):
            curr_node = curr_node.getLeft()
    return curr_node

