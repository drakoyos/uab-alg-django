from Nodo import Nodo


class Lista:
    def __init__(self):
        self.head = None

    def appendHead(self, dato):
        nuevoNodo = Nodo(dato)
        nuevoNodo.next = self.head
        self.head = nuevoNodo

    def show(self):
        current = self.head
        print('[', end=' ')
        while current:
            print(current.data, end=', ')
            current = current.next

        print(']', end=' ')