from multiprocessing import context
from django.shortcuts import render, redirect
from .forms import CreateCurso
from .models import *

def index(request):
    context={'materias': Curso.objects.all()}
    return render(request, "curso/index.html", context)

def create(request):
    if request.method == 'POST':
        form = CreateCurso(request.POST)
        if form.is_valid():
            form.save()
            return redirect('index')
    else:
        form = CreateCurso()
    context = {'form': form}
    return render(request, "curso/create.html", context)

def delete(request, curso_id):
    curso = Curso.objects.get(id=curso_id)
    curso.delete()
    return redirect('index')

def update(request, curso_id):
    curso = Curso.objects.get(id=curso_id)
    if request.method == 'POST':
        form = CreateCurso(request.POST, instance=curso)
        if form.is_valid():
            form.save()
            return redirect('index')
    else:
        form = CreateCurso(instance=curso)
    context = {'form': form}
    return render(request, "curso/edit.html", context)

# def enroll(request):
#     if request.method == 'POST':
#         form = CreateCurso(request.POST)
#         if form.is_valid():
#             form.save()
#             return redirect('index')
#     else:
#         form = CreateCurso()
#     context = {'form': form}
#     return render(request, "curso/inscripcion.html", context)