from django.urls import path
from . import views


urlpatterns = [
    # Estudiantes
    path('estudiantes/', views.index_e),
    path('estudiantes/crear', views.create_e),
    path('estudiantes/edit/<int:id>', views.edit_e),
    path('estudiantes/update/<int:id>', views.update_e),
    path('estudiantes/delete/<int:id>', views.destroy_e),
    # Docentes
    path('docentes/', views.index_d),
    path('docentes/crear', views.create_d),
    path('docentes/edit/<int:id>', views.edit_d),
    path('docentes/update/<int:id>', views.update_d),
    path('docentes/delete/<int:id>', views.destroy_d),
]

