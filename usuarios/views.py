from django.shortcuts import render
from django.http import HttpResponse, HttpResponseRedirect
from usuarios.models import Estudiantes, Docentes
from firstDjango.forms import EstudiantesForm, DocentesForm
from fdj.models import Universidad
from django.shortcuts import render, redirect
from django.urls import reverse
from pprint import pprint

from collections import namedtuple

Pescado = namedtuple("Pez", ["nombre", "especie", "tanque"])

# Crud de Estudiantes
def index_e(request):
    samu = Pescado("Samu", "", "tanque-a")
    estudiantes = Estudiantes.objects.all()
    return render(request, "usuarios/estudiantes/index.html", {'estudiantes': estudiantes,'samu':samu})

def create_e(request):
    if request.method == "POST":
        form = EstudiantesForm(request.POST)
        if form.is_valid():
            try:
                form.save()
                return redirect('/usuarios/estudiantes/')
            except:
                pass
        else:
            return render(request, "usuarios/estudiantes/create.html", {'form': form})
    else:
        form = EstudiantesForm()
    return render(request, 'usuarios/estudiantes/create.html', {'form': form})

def edit_e(request, id):
    estudiante = Estudiantes.objects.get(id=id)
    universidades = Universidad.objects.all()
    format_date = estudiante.fechaDeNacimiento.strftime("%Y-%m-%d")
    return render(request, 'usuarios/estudiantes/edit.html', 
    {
        'estudiante': estudiante,
        'format_date': format_date,
        'universidades': universidades,
    })

def update_e(request, id):
    estudiante = Estudiantes.objects.get(id=id)
    form = EstudiantesForm(request.POST, instance=estudiante)
    if form.is_valid():
        form.save()
        return redirect('/usuarios/estudiantes/')
    else:
        # Redirect back to the same page if the data was invalid
        return render(request, 'usuarios/estudiantes/edit.html', {'estudiante': estudiante, 'form': form})

def destroy_e(request, id):
    estudiante = Estudiantes.objects.get(id=id)
    estudiante.delete()
    return redirect('/usuarios/estudiantes/')

# Crud de Docentes
def index_d(request):
    docentes = Docentes.objects.all()
    return render(request, "usuarios/docentes/index.html", {'docentes': docentes})

def create_d(request):
    pass
    if request.method == "POST":
        form = DocentesForm(request.POST)
        if form.is_valid():
            try:
                form.save()
                return redirect('/usuarios/docentes/')
            except:
                pass
        else:
            return render(request, "usuarios/docentes/create.html", {'form': form})
    else:
        form = DocentesForm()
    return render(request, 'usuarios/docentes/create.html', {'form': form})

def edit_d(request, id):
    docente = Docentes.objects.get(id=id)
    universidades = Universidad.objects.all()
    return render(request, 'usuarios/docentes/edit.html', {'docente': docente,'universidades': universidades,})

def update_d(request, id):
    docente = Docentes.objects.get(id=id)
    form = DocentesForm(request.POST, instance=docente)
    if form.is_valid():
        form.save()
        return redirect('/usuarios/docentes/')
    else:
        # Redirect back to the same page if the data was invalid
        return render(request, 'usuarios/docentes/edit.html', {'docente': docente, 'form': form})

def destroy_d(request, id):
    docente = Docentes.objects.get(id=id)
    docente.delete()
    return redirect('/usuarios/docentes/')

# funcion para inscribir a los alumnos
def inscripcion(request, id):
    pass
